package com.rssparser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Simple XML parser.
 *
 * @author Kyryl Potapenko
 */
public class HandleXML {

    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;

    public volatile boolean parsingComplete = true;
    public static ArrayList<PostData> postDataList = new ArrayList<>();

    public HandleXML(String url) {
        this.urlString = url;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        PostData pdData = null;
        try {
            event = myParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;

                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:

                        if (name.equals("title")) {
                            pdData = new PostData();
                            pdData.postTitle = text;
                        } else if (name.equals("link")) {
                            pdData.postLink = text;
                        } else if (name.equals("pubDate")) {
                            pdData.postDate = text;
                        } else if (name.equalsIgnoreCase("media:thumbnail")) {
                            if (myParser.getAttributeValue(null, "width").equals("144")) {
                                String imageLink = myParser.getAttributeValue(null, "url");
                                pdData.postThumbUrl = imageLink;
                            }
                        }
                        if (!postDataList.contains(pdData)&& !pdData.postTitle.contains("ABC News: Top Stories"))
                            postDataList.add(pdData);
                        break;
                }
                event = myParser.next();
            }
            parsingComplete = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readImage(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "media:thumbnail");
        String imageLink = parser.getAttributeValue(null, "url");
        return imageLink;
    }

    public void fetchXML() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    // Starts the query
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);

                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                }
            }
        });
        thread.start();
    }

}
