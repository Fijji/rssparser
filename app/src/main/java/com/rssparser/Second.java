package com.rssparser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
/**
 * Second activity.
 *
 * @author Kyryl Potapenko
 */
public class Second extends Activity {
    private String lnk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_main);
        WebView w1 = (WebView) findViewById(R.id.webView);
        Intent intent = getIntent();
        lnk = intent.getExtras().getString("linkToLoad");
        w1.loadUrl(lnk);
    }
}