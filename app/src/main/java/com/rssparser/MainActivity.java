package com.rssparser;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import static com.rssparser.HandleXML.postDataList;

/**
 * Main activity.
 *
 * @author Kyryl Potapenko
 */
public class MainActivity extends Activity {
    Button b1;
    ListView lvMain;
    private String finalUrl = "http://feeds.abcnews.com/abcnews/topstories";
    private HandleXML obj;
    final Uri FEED_URI = Uri
            .parse("content://com.rssparser.providers.FeedsList/feeds");

    final String FEED_TITLE = "title";
    final String FEED_LINK = "link";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvMain = (ListView) this.findViewById(R.id.rssListView);
        b1 = (Button) findViewById(R.id.button);

        load();

        final PostItemAdapter itemAdapter = new PostItemAdapter(this, R.layout.item, postDataList);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = new Intent(MainActivity.this, Second.class);
                String link = postDataList.get(position).postLink;
                in.putExtra("linkToLoad", link);
                startActivity(in);
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvMain.setAdapter(itemAdapter);
                obj = new HandleXML(finalUrl);
                obj.fetchXML();
                while (obj.parsingComplete) ;
                itemAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onDestroy() {
        ContentValues cv = new ContentValues();
        for (PostData d : postDataList) {
            cv.put(FEED_TITLE, d.postTitle);
            cv.put(FEED_LINK, d.postLink);
            Uri newUri = getContentResolver().insert(FEED_URI, cv);
        }
        super.onDestroy();
    }

    public void load() {
        Cursor cursor = getContentResolver().query(FEED_URI, null, null,
                null, null);
        if (cursor.moveToFirst()) {
            startManagingCursor(cursor);
            String from[] = {FEED_TITLE, FEED_LINK};
            int to[] = {R.id.titleT, R.id.linkL};
            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                    R.layout.item, cursor, from, to);
            lvMain.setAdapter(adapter);
        }
    }

}