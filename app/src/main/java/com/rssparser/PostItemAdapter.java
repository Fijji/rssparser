package com.rssparser;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class PostItemAdapter extends ArrayAdapter<PostData> {

    private Activity myContext;
    private ArrayList<PostData> datas;
    private LayoutInflater inflater;

    public PostItemAdapter(Context context, int textViewResourceId, ArrayList<PostData> objects) {
        super(context, textViewResourceId, objects);
        myContext = (Activity) context;
        datas = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = myContext.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.item, null);

        ViewHolder viewHolder = new ViewHolder();

        TextView postTitleView = (TextView) rowView.findViewById(R.id.titleT);
        postTitleView.setText(datas.get(position).postTitle);

        TextView postDateView = (TextView) rowView.findViewById(R.id.pubDate);
        postDateView.setText(datas.get(position).postDate);

        TextView postLinkView = (TextView) rowView.findViewById(R.id.linkL);
        postLinkView.setText(datas.get(position).postLink);

        viewHolder.imageView = (ImageView) rowView.findViewById(R.id.list_image);
        rowView.setTag(viewHolder);
        viewHolder = (ViewHolder) rowView.getTag();
        viewHolder.imageURL = datas.get(position).postThumbUrl;
        new DownloadAsyncTask().execute(viewHolder);

        return rowView;
    }

    private static class ViewHolder {
        ImageView imageView;
        public String imageURL;
        public Bitmap bitmap;
    }

    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {

            ViewHolder viewHolder = params[0];
            try {
                URL imageURL = new URL(viewHolder.imageURL);
                viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
            } catch (IOException e) {
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            if (result.bitmap == null) {
                result.imageView.setImageResource(R.drawable.ic_launcher);
            } else {
                result.imageView.setImageBitmap(result.bitmap);
            }
        }
    }
}
